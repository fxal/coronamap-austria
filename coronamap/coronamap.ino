/*********************************************************************
/* Corona Map Austria.
/* Firmware for a physical corona map displaying the warn levels
/* of the states of Austria by parsing official data.
/* Bill of materials: https://gitlab.com/fxal/coronamap-austria/
/* Date: 29.03.2021
/* Author: Felix Almer | www.almer.dev
/* License: https://creativecommons.org/licenses/by/4.0/
/********************************************************************/
#include <Arduino.h>
#include <StreamUtils.h>
#include <Adafruit_NeoPixel.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecureBearSSL.h>
#include <ArduinoJson.h>
#include "secret.h"

#define PIN D1
#define NUMPIXELS 9
#define CORONA_AMPEL_JSON_URL "https://corona-ampel.gv.at/sites/corona-ampel.gv.at/files/assets/Warnstufen_Corona_Ampel_aktuell.json"

// Fingerprint of corona-ampel.gv.at
const uint8_t fingerprint[20] = {0xb4, 0xff, 0x0c, 0x85, 0x18, 0x92, 0x95, 0xed, 0x0d, 0x2d, 0xc0, 0x84, 0xd2, 0xc7, 0x3b, 0xd6, 0x04, 0x13, 0x0c, 0xdb};

ESP8266WiFiMulti WiFiMulti;

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
	Serial.begin(115200);
	// Serial.setDebugOutput(true);
	
	Serial.println();
	Serial.println();
	Serial.println();
	
	for (uint8_t t = 4; t > 0; t--) {
		Serial.printf("[SETUP] WAIT %d...\n", t);
		Serial.flush();
		delay(1000);
	}
	
	WiFi.mode(WIFI_STA);
	WiFiMulti.addAP(WIFI_SSID, WIFI_PASS);
	
	pixels.begin();
	for(uint8_t i = 0; i < 9; i++) {
		pixels.setPixelColor(i, pixels.Color(0, 0, 50));
	}
    pixels.show();
}

void loop() {
	// wait for WiFi connection
	if ((WiFiMulti.run() == WL_CONNECTED)) {
		
		std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);

		client->setFingerprint(fingerprint);
		//client->setInsecure();
		
		HTTPClient https;
		https.useHTTP10(true);
		
		Serial.print("[HTTPS] begin...\n");
		if (https.begin(*client, CORONA_AMPEL_JSON_URL)) {
			
			Serial.print("[HTTPS] GET...\n");
			int httpCode = https.GET();
			Serial.printf("[HTTPS] GET... code: %d\n", httpCode);
			
			if (httpCode == HTTP_CODE_OK) {
				
				DynamicJsonDocument doc(192);
				
				ReadLoggingStream loggingStream(*client, Serial);
				client->find("\"Warnstufen\": [");
				do {
					//DeserializationError error = deserializeJson(doc, loggingStream);
					DeserializationError error = deserializeJson(doc, *client);
					
					if (error) {
						Serial.print(F("deserializeJson() failed: "));
						Serial.println(error.f_str());
						return;
					}
					
					if(strcmp(doc["Region"].as<char*>(), "Bundesland") == 0) {
						Serial.print(doc["Name"].as<char*>());
						Serial.print(": ");
						Serial.println(doc["Warnstufe"].as<char*>());
						
						parseName(doc["Name"].as<char*>(), doc["Warnstufe"].as<int>());
					}
				} while(client->findUntil(",", "]"));
				pixels.show();

				Serial.println();
				Serial.print("[HTTPS] connection closed or file end.\n");
				
			} else {
				Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
			}
			
			https.end();
			
			} else {
			Serial.printf("[HTTPS] Unable to connect\n");
		}
		Serial.println("Wait 30m before next round...");
		delay(1800000);
	}
	delay(3000);
}

void warnColor(int color, int pixel) {
	switch(color) {
		case 1:
			// Lowest warning level: green
			pixels.setPixelColor(pixel, pixels.Color(0, 50, 0));
			break;
		case 2:
			// Low warning level: yellow
			pixels.setPixelColor(pixel, pixels.Color(20, 30, 0));
			break;
		case 3:
			// Medium warning level: orange
			pixels.setPixelColor(pixel, pixels.Color(40, 10, 0));
			break;
		case 4:
			// Highest warning level: red
			pixels.setPixelColor(pixel, pixels.Color(50, 0, 0));
			break;
		default:
			// Invalid value, turn off
			pixels.setPixelColor(pixel, pixels.Color(0, 0, 0));
			break;
	}
}

void parseName(const char* name, int warnLevel) {
	if(strcmp(name, "Oberösterreich") == 0) {
		warnColor(warnLevel, 0);
	} else	if(strcmp(name, "Niederösterreich") == 0) {
		warnColor(warnLevel, 1);
	} else if(strcmp(name, "Wien") == 0) {
		warnColor(warnLevel, 2);
	} else if(strcmp(name, "Burgenland") == 0) {
		warnColor(warnLevel, 3);
	} else if(strcmp(name, "Steiermark") == 0) {
		warnColor(warnLevel, 4);
	} else if(strcmp(name, "Kärnten") == 0) {
		warnColor(warnLevel, 5);
	} else if(strcmp(name, "Salzburg") == 0) {
		warnColor(warnLevel, 6);
	} else if(strcmp(name, "Tirol") == 0) {
		warnColor(warnLevel, 7);
	} else if(strcmp(name, "Vorarlberg") == 0) {
		warnColor(warnLevel, 8);
	}
}
