# Corona status map Austria
This project displays real corona status data provided by https://corona-ampel.gv.at/ using LEDs.
All nine states of *Austria* (as well as every county within Austria) have a warning level: criticality 1, the lowest, to 4, the highest.
These levels are provided as JSON by https://www.data.gv.at/ for free use under [Creative Commons](https://creativecommons.org/licenses/by/4.0/deed.de) license and can be found [here](https://www.data.gv.at/katalog/dataset/52abfc2b-031c-4875-b838-653abbfccf4e).

![](images/map_complete.jpg)

A total of nine LEDs display the current warning level of each state with the following colors:
- *Red* for the highest level 4
- *Orange* for level 3
- *Yellow* for level 2 and
- *Green* for level 1

An additional color *blue* is the default to indicate no data has yet been received.

![](images/all_colors.jpg)

## Input data
The JSON data provided by https://corona-ampel.gv.at/ contains individual ratings of all available areas, where each area entry looks like this:
```json
{
	"Region": "Bundesland",
	"GKZ": "5",
	"Name": "Salzburg",
	"Warnstufe": "4"
}
```
All previous ratings are also found within the JSON data, making it difficult for micro controllers to be parsed due to the relatively large size of the HTTP GET response.
Therefore only the latest entries are parsed by the [ArduinoJson](https://arduinojson.org/) library. Also parsing is done in [chunks](https://arduinojson.org/v6/how-to/deserialize-a-very-large-document/) by the library for memory preservation reasons.

## Hardware in use
An esp8266 D1 Mini handles the scraping of the data, parsing the JSON and displaying the colors of each state.

Nine WS2812B LEDs are daisy chained and connected to the esp8266 as follows:

esp8266 3V3 -> Power In WS2812B

esp8266 D1 -> Data In WS2812B

esp8266 GND -> GND WS2812B

![](images/back_progress.jpg)

## Bill of materials
These materials are required for building the corona map are as follows:

- 1x ESP8266 D1 Mini (or compatible)
- 9x WS2812B single LED components
- Wire
- Solder & Solder station
- Picture frame
- Printer (for printing the country border page)
- Tape

## License
This work is licensed under [Creative Commons Attribution 4.0](https://creativecommons.org/licenses/by/4.0/legalcode).